//
//  MusicPlayerAdmin.swift
//  MusicPlayerTips
//
//  Created by See.Ku on 2015/06/05.
//  Copyright (c) 2015 AxeRoad. All rights reserved.
//

import UIKit
import MediaPlayer

class MusicPlayerAdmin: SK4MusicPlayerAdmin {

	weak var viewController: ViewController?

	/// アプリがフォアグラウンドになる
	override func onWillEnterForeground() {
		viewController?.updateOperation()
	}

	/// アイテムが変更になった
	override func onPlayItemChanged() {
		viewController?.updatePlayInfo()
	}

	/// 再生状況が変化した
	override func onPlaybackStateChanged() {
		viewController?.updatePlayState()
	}

	////////////////////////////////////////////////////////////////

	/// プレイリストを更新
	func updatePlaylist() {
		let query = MPMediaQuery.songsQuery()

		// クラウドにあるアイテムを除外
		let num = NSNumber(bool: false)
		let pre = MPMediaPropertyPredicate(value: num, forProperty: MPMediaItemPropertyIsCloudItem)
		query.addFilterPredicate(pre)

		if var all_ar = query.items as? [MPMediaItem] {

			// 再生回数と曲名でソート
			all_ar.sort() { s0, s1 in
				if s0.playCount == s1.playCount {

					// FIXME: 間違ってはいないが正しくもない

					return s0.title < s1.title

				} else {
					return s0.playCount < s1.playCount
				}
			}

			// 最大で100件取り出し
			let no = min(100, all_ar.count)
			var ar = [MPMediaItem](all_ar[0..<no])

			// 必要ならプレイリストをシャッフル
			if shuffleFlag {
				shufflePlaylist(&ar)
			}

			playlist = ar
		}
	}

	/// プレイリストをシャッフル
	func shufflePlaylist(inout array: [MPMediaItem]) {
		let max = array.count
		for no in (0...max*2) {
			let i = no % max
			let j = Int(arc4random_uniform(UInt32(max)))
			if i != j {
				swap(&array[i], &array[j])
			}
		}
	}

}

// eof
