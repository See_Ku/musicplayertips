//
//  PlaylistTableAdmin.swift
//  MusicPlayerTips
//
//  Created by See.Ku on 2015/06/06.
//  Copyright (c) 2015 AxeRoad. All rights reserved.
//

import UIKit
import MediaPlayer

class PlaylistTableAdmin: NSObject, UITableViewDelegate, UITableViewDataSource {

	func setup(tableView: UITableView) {
		tableView.delegate = self
		tableView.dataSource = self
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return g_musicPlayer.playlist.count
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cellId = "Cell"

		let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! UITableViewCell
		let item = g_musicPlayer.playlist[indexPath.row]

		cell.textLabel?.text = item.title
		cell.detailTextLabel?.text = "\(item.artist) - \(item.albumTitle)"
		cell.imageView?.image = getArtworkImage(item)

		return cell
	}

	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

		// タッチされたセルの曲を再生
		g_musicPlayer.play(indexPath.row)

		// 選択を解除しておく
		tableView.deselectRowAtIndexPath(indexPath, animated: true)
	}

	////////////////////////////////////////////////////////////////

	/// アートワークで使用するイメージを取得
	func getArtworkImage(item: MPMediaItem) -> UIImage? {

		// 再生中の曲の場合は専用のイメージを返す
		if g_musicPlayer.nowPlayingItem == item {
			return UIImage(named: "playing")
		}

		// 曲に設定されてるアートワークを取得
		let size = CGSize(width: 40, height: 40)
		if let artwork = item.artwork {
			return artwork.imageWithSize(size)
		}

		return nil
	}
	
}

// eof
