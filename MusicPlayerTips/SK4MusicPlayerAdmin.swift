//
//  SK4MusicPlayerAdmin.swift
//  MusicPlayerTips
//
//  Created by See.Ku on 2015/06/05.
//  Copyright (c) 2015 AxeRoad. All rights reserved.
//

import Foundation
import MediaPlayer
import AVFoundation

/// MPMusicPlayerControllerを管理するためのクラス
class SK4MusicPlayerAdmin: NSObject {

	////////////////////////////////////////////////////////////////
	// MARK: - 初期化＆プロパティ

	/// アプリ全体で使用するMPMusicPlayerController
	let player = MPMusicPlayerController.systemMusicPlayer()

	override init() {
		super.init()

		registAllObserver()
	}

	deinit {
		removeAllObserver()
	}

	////////////////////////////////////////////////////////////////
	// MARK: - 通知

	func registAllObserver() {
		let nc = NSNotificationCenter.defaultCenter()
		nc.addObserver(self, selector: "didEnterBackground:", name: UIApplicationDidEnterBackgroundNotification, object: nil)
		nc.addObserver(self, selector: "willEnterForeground:", name: UIApplicationWillEnterForegroundNotification, object: nil)
		nc.addObserver(self, selector: "playItemChanged:", name: MPMusicPlayerControllerNowPlayingItemDidChangeNotification, object: player)
		nc.addObserver(self, selector: "playbackStateChanged:", name: MPMusicPlayerControllerPlaybackStateDidChangeNotification, object: player)

		player.beginGeneratingPlaybackNotifications()
	}

	func removeAllObserver() {
		player.endGeneratingPlaybackNotifications()

		let nc = NSNotificationCenter.defaultCenter()
		nc.removeObserver(self, name: UIApplicationDidEnterBackgroundNotification, object: nil)
		nc.removeObserver(self, name: UIApplicationWillEnterForegroundNotification, object: nil)
		nc.removeObserver(self, name: MPMusicPlayerControllerNowPlayingItemDidChangeNotification, object: player)
		nc.removeObserver(self, name: MPMusicPlayerControllerPlaybackStateDidChangeNotification, object: player)
	}

	////////////////////////////////////////////////////////////////

	func didEnterBackground(notify: NSNotification) {
		onDidEnterBackground()
	}

	func willEnterForeground(notify: NSNotification) {
		onWillEnterForeground()
	}

	func playItemChanged(notify: NSNotification) {
		onPlayItemChanged()
	}

	/// playbackStateChangedの実行回数
	var stateChangeCount = 0

	func playbackStateChanged(notify: NSNotification) {

		// 通知の直後はAVAudioSessionで再生状況が取れないので、
		// 少し遅らせてチェックする
		let time = dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC / 10))
		let queue = dispatch_get_main_queue()

		// 今回の実行回数を保存
		let count = ++stateChangeCount

		dispatch_after(time, queue) { [weak self] in

			// このループの実行回数が現在の値と一致した時だけ、処理を実行
			if let tmp = self where tmp.stateChangeCount == count {
				tmp.onPlaybackStateChanged()
			}
		}
	}

	////////////////////////////////////////////////////////////////
	// MARK: - イベント対応

	/// アプリがバックグラウンドになった
	func onDidEnterBackground() {
//		println(__FUNCTION__)
	}

	/// アプリがフォアグラウンドになる
	func onWillEnterForeground() {
//		println(__FUNCTION__)
	}

	/// アイテムが変更になった
	func onPlayItemChanged() {
//		println(__FUNCTION__)
	}

	/// 再生状況が変化した
	func onPlaybackStateChanged() {
//		println(__FUNCTION__)
	}

	////////////////////////////////////////////////////////////////
	// MARK: - プレイリスト／再生キュー

	/// アプリで作成したプレイリスト
	var playlist = [MPMediaItem]() {
		didSet {
			playlistToQueue()
		}
	}

	/// プレイリストを再生キューにセット
	func playlistToQueue() {

		// とりあえず停止
		stop()

		// プレイリストに曲が存在する場合だけセット
		if playlist.isEmpty == false {
			let col = MPMediaItemCollection(items: playlist)
			player.setQueueWithItemCollection(col)
		}
	}

	/// すべての曲を再生キューにセット
	func allSongsToQueue() {
		let query = MPMediaQuery.songsQuery()
		player.setQueueWithQuery(query)
	}

	////////////////////////////////////////////////////////////////
	// MARK: - 基本操作

	/// 再生
	func play() {
		player.play()
	}

	/// プレイリストの指定された曲を再生
	func play(no: Int) {
		if 0 <= no && no < playlist.count {
			player.nowPlayingItem = playlist[no]
			play()
		}
	}

	/// 一時停止
	func pause() {
		player.pause()
	}

	/// 停止
	func stop() {
		player.stop()
	}

	/// 1つ前の曲に移動
	func skipToPreviousItem() {
		player.skipToPreviousItem()
	}

	/// 1つ後の曲に移動
	func skipToNextItem() {
		player.skipToNextItem()
	}

	////////////////////////////////////////////////////////////////
	// MARK: - リピート／シャッフル

	/// リピートの状態を取得／設定
	var repeatFlag: Bool {
		get {
			if player.repeatMode == .All {
				return true
			} else {
				return false
			}
		}

		set {
			if newValue {
				player.repeatMode = .All
			} else {
				player.repeatMode = .None
			}
		}
	}

	/// シャッフルの状態を取得／設定
	var shuffleFlag: Bool = false

	////////////////////////////////////////////////////////////////
	// MARK: - その他

	/// 再生中の曲の情報
	var nowPlayingItem: MPMediaItem? {
		get {
			return player.nowPlayingItem
		}

		set {
			player.nowPlayingItem = newValue
		}
	}

	/// 再生中か？
	func isPlaying() -> Bool {

		// player.playbackStateは信頼出来ないので、
		// AVAudioSessionで再生状況を調べる
		let av = AVAudioSession.sharedInstance()
		return av.otherAudioPlaying
	}

}

// eof
