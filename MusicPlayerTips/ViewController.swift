//
//  ViewController.swift
//  MusicPlayerTips
//
//  Created by See.Ku on 2015/06/04.
//  Copyright (c) 2015 AxeRoad. All rights reserved.
//

import UIKit

let g_musicPlayer = MusicPlayerAdmin()

class ViewController: UIViewController {

	@IBAction func onRepeat(sender: AnyObject) {

		// リピートのフラグを反転
		let flag = g_musicPlayer.repeatFlag
		g_musicPlayer.repeatFlag = !flag

		// 画面を更新
		updateOperation()
	}

	@IBAction func onPrev(sender: AnyObject) {
		g_musicPlayer.skipToPreviousItem()
	}

	@IBAction func onPause(sender: AnyObject) {
		g_musicPlayer.pause()
	}

	@IBAction func onPlay(sender: AnyObject) {
		g_musicPlayer.play()
	}

	@IBAction func onNext(sender: AnyObject) {
		g_musicPlayer.skipToNextItem()
	}

	@IBAction func onShuffle(sender: AnyObject) {

		// シャッフルのフラグを反転
		let flag = g_musicPlayer.shuffleFlag
		g_musicPlayer.shuffleFlag = !flag

		// プレイリストを更新
		g_musicPlayer.updatePlaylist()

		// 画面を更新
		updatePlayInfo()
		updateOperation()
	}

	////////////////////////////////////////////////////////////////

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var detailLabel: UILabel!

	/// 再生中の曲の情報を表示
	func updatePlayInfo() {
		if let item = g_musicPlayer.nowPlayingItem {
			titleLabel.text = item.title
			detailLabel.text = "\(item.artist) - \(item.albumTitle)"
		} else {
			titleLabel.text = "MusicPlayerTips"
			detailLabel.text = ""
		}

		// テーブルビューも更新
		playlistTable.reloadData()
	}

	@IBOutlet weak var pauseButton: UIBarButtonItem!
	@IBOutlet weak var playButton: UIBarButtonItem!

	/// 再生状況を画面に反映
	func updatePlayState() {
		let flag = g_musicPlayer.isPlaying()
		pauseButton.enabled = flag
		playButton.enabled = !flag
	}

	@IBOutlet weak var repeatButton: UIBarButtonItem!
	@IBOutlet weak var shuffleButton: UIBarButtonItem!

	/// 各種操作に対応するUIを更新
	func updateOperation() {

		// 有効：　viewと同じtintColor
		// 無効：　tintColorをlightGrayColor

		// リピートボタンに内部の設定を反映
		if g_musicPlayer.repeatFlag {
			repeatButton.tintColor = view.tintColor
		} else {
			repeatButton.tintColor = UIColor.lightGrayColor()
		}

		// シャッフルボタンに内部の設定を反映
		if g_musicPlayer.shuffleFlag {
			shuffleButton.tintColor = view.tintColor
		} else {
			shuffleButton.tintColor = UIColor.lightGrayColor()
		}
	}

	////////////////////////////////////////////////////////////////

	/// プレイリストを表示するTableView
	@IBOutlet weak var playlistTable: UITableView!
	var playlistTableAdmin: PlaylistTableAdmin!

	override func viewDidLoad() {
		super.viewDidLoad()

//		g_musicPlayer.allSongsToQueue()
		g_musicPlayer.updatePlaylist()
		g_musicPlayer.viewController = self

//		for (no, item) in enumerate(g_musicPlayer.playlist) {
//			println("\(no+1): \(item.title)")
//		}

		updatePlayInfo()
		updatePlayState()
		updateOperation()

		// TableView管理用のクラスを生成＆設定
		playlistTableAdmin = PlaylistTableAdmin()
		playlistTableAdmin.setup(playlistTable)
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

}

// eof
